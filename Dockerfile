FROM ubuntu:16.04

EXPOSE 8080

RUN apt-get update

RUN apt-get install -y curl

RUN curl --silent --location https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install --yes nodejs

RUN mkdir files

COPY . /files

RUN npm install -g crud-file-server

CMD ["crud-file-server", "-f", "files"]